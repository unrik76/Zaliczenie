using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptNr1 : MonoBehaviour
{
    [Header("Game Object")]
        public GameObject Cube;
        private GameObject spawnedCube;
    [Header("Lenght Of Fibbonachi")]
        public float Fibbonachi;

    [Header("This is responsible for wave")]
        [Tooltip("Hight of the wave")]
         public float Amplitude;

        [Tooltip("Distance between waves")]
            public float Frequency;

        [Tooltip("Amount of cubes in one wave")]
            public float WaveCubeAmount;

    [Header("This is responsible for snail")]
        [Tooltip("how long should the snail be")]
            public float SnailLength;
    [Header("This is responsible for circle")]
    public float CircleRadius;
    public float CircleCube;



    private void OnEnable()
    {
        //Kod Fibbonachiego
        int sus = 0;
        for (int i = 0; i < Fibbonachi; i++)
        {
            for (int z = 0; z < Fibo(i); z++)
            {
                var posision = new Vector3(sus++, 0f, 4f);
                spawnedCube = Instantiate(Cube, posision, Quaternion.identity, transform);
            }
            sus++;
        }
        //Fala Sinusoidalna
        for (int i = 0; i < WaveCubeAmount; i++)
        {
            var posision = new Vector3(i * Frequency, waveFuction(i) * Amplitude, 2f);
            spawnedCube = Instantiate(Cube, posision, Quaternion.identity, transform);
        }
        //slimak
        float xPos = 0;
        float yPos = 0;
        float increm = 0;
        /*
         for (int i = 0; i < SnailLength; i++)
         {
             for (int z = 0; z < increm; z++)
             {
                 var posision = new Vector3(xPos, yPos, 6f);
                 spawnedCube = Instantiate(Cube, posision, Quaternion.identity, transform);
                 xPos++;
             }
             increm++;

             for (int z = 0; z < increm; z++)
             {
                 var posision = new Vector3(xPos, yPos, 6f);
                 spawnedCube = Instantiate(Cube, posision, Quaternion.identity, transform);
                 yPos++;
             }
             increm++;
             for (int z = 0; z < increm; z++)
             {
                 var posision = new Vector3(xPos, yPos, 6f);
                 spawnedCube = Instantiate(Cube, posision, Quaternion.identity, transform);
                 xPos--;
             }
             increm++;
             for (int z = 0; z < increm; z++)
             {
                 var posision = new Vector3(xPos, yPos, 6f);
                 spawnedCube = Instantiate(Cube, posision, Quaternion.identity, transform);
                 yPos--;
             }
             increm++;
         }
        */
        float limit = 1;
        float stub = 0;
        for (int i = 0; i < SnailLength; i++)
        {
            // [-- This is responsible for one by one addision and subtracktion of X and Y --]
            switch (stub)
            {
                case (0) :  yPos++;
                    break ;
                case (1):   xPos++;
                    break;
                case (2):   yPos--;
                    break;
                case (3):   xPos--;
                    break;
            }
            //[-- This is is responsible for "Turning" the snake, making it so it's possible for a gap to exist and not making the snake into a big block --]
            if (increm == limit)
                {
                    if (stub == 3)
                    {
                        increm = 0;
                        stub = 0;
                    }
                    else
                    {
                        increm = 0;
                        stub++;
                    }
                 limit = limit + 1;
         }
            increm++;
            //[-- This is responsible for showing me the coordinats on the map, purely for me to mke it easyer to trouble shoot any problems. --]
            Debug.Log("Posision X:"+xPos +" Posision Y:"+ yPos);
            var posision = new Vector3(xPos, yPos-1, 6f);
            spawnedCube = Instantiate(Cube, posision, Quaternion.identity, transform);
        }

        xPos = 0;
        yPos = 0;
        float rad = 0;
        for (int i = 0; i < CircleCube; i++)
        {
            //[-- Using these trigonometrical aquasion, the computer knows where to put the block --]
            yPos = Mathf.Cos(rad) * CircleRadius;
            xPos = Mathf.Sin(rad) * CircleRadius;
            //[-- This makes things move forward, dependent on the amount of cubes --]
            rad = rad + (2 * 3.14159265359f / CircleCube);

            var posision = new Vector3(xPos, yPos, 8f);
            spawnedCube = Instantiate(Cube, posision, Quaternion.identity, transform);

        }


    }
    private void OnDisable()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }
    private float Fibo(int f)
    {
        int a = 0;
        int b = 1;
        int c = 1;
        for (int i = 0; i < f; i++)
        {
            c = a + b;
            a = b;
            b = c;
        }
        return c;
    }
    private float waveFuction(float x)
    {
        return Mathf.Sin(x) ;
    }
}
