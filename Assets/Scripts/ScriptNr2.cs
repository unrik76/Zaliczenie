using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptNr2 : MonoBehaviour
{
    [Header("Game Object")]
        public GameObject Cube;
        private Material mater;

    [Header("Grid Size")]
        public float GridLength;
        public float GridHight;
        private Color Coloreq;

    private void Start()
    {

        float xPos = 0;
        float yPos = 0;

        for (int i = 0; i < GridHight; i++)
        {
            xPos = 0;
                for (int z = 0; z < GridLength; z++)
                {
                    var posision = new Vector3(xPos, yPos, -2f);
                    var spawnedCube = Instantiate(Cube, posision, Quaternion.identity, transform);
                if (z == 0) {
                    Coloreq = new Color(1, 1, 1, 1);
                }
                else
                {
                    Coloreq = new Color(1, 1 - z / (GridLength  -1) ,1 - z / (GridLength - 1), 1);
                }
                        spawnedCube.GetComponent<MeshRenderer>().material.color = Coloreq;

                    xPos = xPos + 1.1f;
                }
            yPos = yPos + 1.1f;
        }
        var nextPos = yPos;
        xPos = 0;
        yPos = 0;

        for (int i = 0; i < GridHight; i++)
        {
            xPos = 0;
            for (int z = 0; z < GridLength; z++)
            {
                var posision = new Vector3(xPos, yPos + nextPos + 1, -2f);
                var spawnedCube = Instantiate(Cube, posision, Quaternion.identity, transform);
                if (z == 0)
                {
                    Coloreq = new Color(1, 1, 1, 1);
                }
                else if(z <= GridLength / 2)
                {
                    Coloreq = new Color(1, 1 - (z / (GridLength - 1)) * 2, 1 - (z / (GridLength - 1)) * 2, 1);
                }
                else
                {
                    Coloreq = new Color(1 - (z - (GridLength / 2)) / (GridLength - 1) * 2, 0, 0, 1);
                }
                spawnedCube.GetComponent<MeshRenderer>().material.color = Coloreq;

                xPos = xPos + 1.1f;
            }
            yPos = yPos + 1.1f;
        }
        
        xPos = 0;
        yPos = 0;

        var increm = 1;

        for (int i = 0; i < 8; i++)
        {
            xPos = 0;
            for (int z = 0; z < 8; z++)
            {
                var posision = new Vector3(xPos, yPos, -4f);
                var spawnedCube = Instantiate(Cube, posision, Quaternion.identity, transform);
                if (increm == 1)
                {
                    Coloreq = new Color(0, 0, 0, 1);
                }
                else
                {
                    Coloreq = new Color(1, 1, 1, 1);
                }
                spawnedCube.GetComponent<MeshRenderer>().material.color = Coloreq;
                increm = increm * (-1);
                xPos++;
            }
            increm = increm * (-1);
            yPos++;
        }


    }

    private void OnDisable()
        {
            foreach (Transform child in transform)
            {
                Destroy(child.gameObject);
            }
        }

}
